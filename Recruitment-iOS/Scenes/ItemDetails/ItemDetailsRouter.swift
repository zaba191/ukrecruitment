//
//	ItemDetailsRouter.swift
//	Recruitment-iOS
//
//	Created by Bartłomiej Zabicki on 03/08/2019.
//	Copyright (c) 2019 Untitled Kingdom. All rights reserved.
//

import UIKit

@objc protocol ItemDetailsRoutingLogic {}

protocol ItemDetailsDataPassing {
	var dataStore: ItemDetailsDataStore? { get }
}

class ItemDetailsRouter: NSObject, ItemDetailsRoutingLogic, ItemDetailsDataPassing {

	// MARK: - Properties

	weak var viewController: ItemDetailsViewController?
	var dataStore: ItemDetailsDataStore?
	
	// MARK: - Routing
	
}
