//
//	ItemDetailsModels.swift
//	Recruitment-iOS
//
//	Created by Bartłomiej Zabicki on 03/08/2019.
//	Copyright (c) 2019 Untitled Kingdom. All rights reserved.
//

import UIKit

enum ItemDetails {
    
    enum FetchDetails {
        struct WorkerRequest {
            let id: String
        }
        struct Response {
            let details: ItemDetailsModel
        }
        struct ErrorResponse {
            let error: AppError
        }
        struct ViewModel {
            let description: String
        }
        struct ErrorViewModel {
            let message: String
        }
    }
    
    enum PrepareItem {
        struct Response {
            let item: ItemModel
        }
        struct ViewModel {
            let title: String
            let backgroundColor: UIColor
        }
    }
    
}
