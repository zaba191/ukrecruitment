//
//	ItemDetailsPresenter.swift
//	Recruitment-iOS
//
//	Created by Bartłomiej Zabicki on 03/08/2019.
//	Copyright (c) 2019 Untitled Kingdom. All rights reserved.
//

import UIKit

protocol ItemDetailsPresentationLogic {
    func presentFetchDetails(response: ItemDetails.FetchDetails.Response)
    func presentFetchDetailsError(response: ItemDetails.FetchDetails.ErrorResponse)
    func presentItem(response: ItemDetails.PrepareItem.Response)
}

class ItemDetailsPresenter: ItemDetailsPresentationLogic {
    
    // MARK: - Properties
    
    weak var viewController: ItemDetailsDisplayLogic?
    
    // MARK: - Presentation Logic
    
    func presentFetchDetails(response: ItemDetails.FetchDetails.Response) {
        
        let viewModel = ItemDetails.FetchDetails.ViewModel(description: response.details.desc)
        viewController?.displayFetchDetails(viewModel: viewModel)
    }
    
    func presentFetchDetailsError(response: ItemDetails.FetchDetails.ErrorResponse) {
        viewController?.displayFetchDetailsError(viewModel: .init(message: response.error.localizedDescription))
    }
    
    func presentItem(response: ItemDetails.PrepareItem.Response) {
        let formattedtitle = response.item.name.uppercasedLetter(every: 2)
        
        let viewModel = ItemDetails.PrepareItem.ViewModel(title: formattedtitle,
                                                          backgroundColor: response.item.color)
        viewController?.displayItem(viewModel: viewModel)
    }
    
}
