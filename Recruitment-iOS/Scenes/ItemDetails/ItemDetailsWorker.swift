//
//	ItemDetailsWorker.swift
//	Recruitment-iOS
//
//	Created by Bartłomiej Zabicki on 03/08/2019.
//	Copyright (c) 2019 Untitled Kingdom. All rights reserved.
//

import UIKit
import Unicorns

class ItemDetailsWorker {
    
    func fetchItemDetails(request: ItemDetails.FetchDetails.WorkerRequest,
                          onSuccess: @escaping (ItemDetailsModel) -> Void,
                          onError: @escaping (AppError) -> Void) {
        
        let endpoint = Endpoint.item(id: request.id)
        Networking.session = MockURLSession(expectedJSON: JSONMocksManager.json(from: endpoint.path),
                                            expectedStatusCode: 200)
        Networking.shared.request(endpoint: endpoint, onSuccess: onSuccess, onError: onError)
    }
    
}
