//
//	ItemDetailsInteractor.swift
//	Recruitment-iOS
//
//	Created by Bartłomiej Zabicki on 03/08/2019.
//	Copyright (c) 2019 Untitled Kingdom. All rights reserved.
//

import UIKit

protocol ItemDetailsBusinessLogic {
    func fetchDetails()
    func prepareView()
}

protocol ItemDetailsDataStore {
    var item: ItemModel! { get set }
}

class ItemDetailsInteractor: ItemDetailsBusinessLogic, ItemDetailsDataStore {
    
    // MARK: - Properties
    
    var presenter: ItemDetailsPresentationLogic?
    private let worker = ItemDetailsWorker()
    var item: ItemModel!
    
    // MARK: - Business Logic
    
    func fetchDetails() {
        
        let workerRequest = ItemDetails.FetchDetails.WorkerRequest(id: item.id)
        worker.fetchItemDetails(request: workerRequest, onSuccess: { [weak self] details in
            guard let self = self else { return }
            self.presenter?.presentFetchDetails(response: .init(details: details))
            }, onError: { [weak self] error in
                guard let self = self else { return }
                self.presenter?.presentFetchDetailsError(response: .init(error: error))
        })
    }
    
    func prepareView() {
        presenter?.presentItem(response: .init(item: item))
    }
    
}
