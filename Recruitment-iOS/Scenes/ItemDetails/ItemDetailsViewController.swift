//
//	ItemDetailsViewController.swift
//	Recruitment-iOS
//
//	Created by Bartłomiej Zabicki on 03/08/2019.
//	Copyright (c) 2019 Untitled Kingdom. All rights reserved.
//

import UIKit

protocol ItemDetailsDisplayLogic: class {
    func displayFetchDetails(viewModel: ItemDetails.FetchDetails.ViewModel)
    func displayFetchDetailsError(viewModel: ItemDetails.FetchDetails.ErrorViewModel)
    func displayItem(viewModel: ItemDetails.PrepareItem.ViewModel)
}

class ItemDetailsViewController: UIViewController, ItemDetailsDisplayLogic {
    
    // MARK: - Outlets
    
    @IBOutlet weak var descriptionTextView: UITextView!

	// MARK: - Properties

	var interactor: ItemDetailsBusinessLogic?
	var router: (NSObjectProtocol & ItemDetailsRoutingLogic & ItemDetailsDataPassing)?

	// MARK: - Initialization
	
	override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
		super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
		setup()
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		setup()
	}
	
	// MARK: - Setup
	
	private func setup() {
		let viewController = self
		let interactor = ItemDetailsInteractor()
		let presenter = ItemDetailsPresenter()
		let router = ItemDetailsRouter()
		viewController.interactor = interactor
		viewController.router = router
		interactor.presenter = presenter
		presenter.viewController = viewController
		router.viewController = viewController
		router.dataStore = interactor
	}
	
	// MARK: - Routing
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if let scene = segue.identifier {
			let selector = NSSelectorFromString("routeTo\(scene)WithSegue:")
			if let router = router, router.responds(to: selector) {
				router.perform(selector, with: segue)
			}
		}
	}
	
	// MARK: - Lifecycle
	
	override func viewDidLoad() {
		super.viewDidLoad()
        interactor?.prepareView()
        interactor?.fetchDetails()
	}

	// MARK: - Display Logic
    
    func displayFetchDetails(viewModel: ItemDetails.FetchDetails.ViewModel) {
        descriptionTextView.text = viewModel.description
    }
    
    func displayFetchDetailsError(viewModel: ItemDetails.FetchDetails.ErrorViewModel) {
        descriptionTextView.text = viewModel.message
    }
    
    func displayItem(viewModel: ItemDetails.PrepareItem.ViewModel) {
        title = viewModel.title
        view.backgroundColor = viewModel.backgroundColor
    }
	
}
