//
//  ItemsGridCollectionViewDelegate+DataSource.swift
//  Recruitment-iOS
//
//  Created by Bartłomiej Zabicki on 03/08/2019.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

import UIKit

protocol ItemGridCollectionViewDelegate: class {
    func didSelect(item: ItemModel)
}

final class ItemsGridCollectionViewDelegateDataSource: NSObject, UICollectionViewDataSource {
    
    // MARK: - Properties
    
    var items: [ItemModel]
    weak var delegate: ItemGridCollectionViewDelegate?
    
    // MARK: - Initialization
    
    init(items: [ItemModel], delegate: ItemGridCollectionViewDelegate) {
        self.items = items
        self.delegate = delegate
    }
    
    // MARK: - UICollectionViewDataSource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusable(cell: ItemCollectionViewCell.self, for: indexPath)
        let item = items[indexPath.row]
        
        let viewModel = ItemCollectionViewCell.ViewModel(title: item.name,
                                                    backgroundColor: item.color)
        cell.setupView(with: viewModel)
        return cell
    }
}

// MARK: - UITableViewDelegate

extension ItemsGridCollectionViewDelegateDataSource: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let selectedItem = items[indexPath.row]
        delegate?.didSelect(item: selectedItem)
    }
    
}

