//
//	ItemsGridPresenter.swift
//	Recruitment-iOS
//
//	Created by Bartłomiej Zabicki on 03/08/2019.
//	Copyright (c) 2019 Untitled Kingdom. All rights reserved.
//

import UIKit

protocol ItemsGridPresentationLogic {
    func presentFetchItems(response: ItemsGrid.FetchItems.Response)
    func presentFetchItemsError(response: ItemsGrid.FetchItems.ErrorResponse)
    func presentSelectedItem()
}

class ItemsGridPresenter: ItemsGridPresentationLogic {

	// MARK: - Properties

	weak var viewController: ItemsGridDisplayLogic?
	
	// MARK: - Presentation Logic
    
    func presentFetchItems(response: ItemsGrid.FetchItems.Response) {
        let delegateDataSource = ItemsGridCollectionViewDelegateDataSource(items: response.items,
                                                                     delegate: response.delegate)
        let viewModel = ItemsGrid.FetchItems.ViewModel(delegateDataSource: delegateDataSource)
        viewController?.displayFetchItems(viewModel: viewModel)
    }
    
    func presentFetchItemsError(response: ItemsGrid.FetchItems.ErrorResponse) {
        viewController?.displayFetchItemsError(viewModel: .init(message: response.error.localizedDescription))
    }
    
    func presentSelectedItem() {
        viewController?.displaySelectedItem()
    }

}
