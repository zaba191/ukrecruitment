//
//	ItemsGridInteractor.swift
//	Recruitment-iOS
//
//	Created by Bartłomiej Zabicki on 03/08/2019.
//	Copyright (c) 2019 Untitled Kingdom. All rights reserved.
//

import UIKit

protocol ItemsGridBusinessLogic {
    func fetchItems()
    func assign(viewModel: ItemsGrid.FetchItems.ViewModel)
}

protocol ItemsGridDataStore: ItemsPersitenceStorable {
    var selectedItem: ItemModel? { get set }
}

class ItemsGridInteractor: ItemsGridBusinessLogic, ItemsGridDataStore {
    
    // MARK: - Properties
    
    var presenter: ItemsGridPresentationLogic?
    private var itemsCollectionViewDelegateDataSource: ItemsGridCollectionViewDelegateDataSource?
    
    var selectedItem: ItemModel?
    var itemsPersistenceStore: ItemsPersistenceStore!
    
    // MARK: - Business Logic
    
    func fetchItems() {
        itemsPersistenceStore.observe { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let items):
                let response = ItemsGrid.FetchItems.Response(items: items, delegate: self)
                self.presenter?.presentFetchItems(response: response)
            case .failure(let error):
                self.presenter?.presentFetchItemsError(response: .init(error: error))
            }
        }
    }
    
    func assign(viewModel: ItemsGrid.FetchItems.ViewModel) {
        itemsCollectionViewDelegateDataSource = viewModel.delegateDataSource
    }
    
    // MARK: - ItemsPersitenceStorable
    
    func assign(persistenceStore: ItemsPersistenceStore) {
        self.itemsPersistenceStore = persistenceStore
    }
    
}

extension ItemsGridInteractor: ItemGridCollectionViewDelegate {
    
    func didSelect(item: ItemModel) {
        selectedItem = item
        presenter?.presentSelectedItem()
    }
    
}
