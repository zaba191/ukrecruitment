//
//	ItemsGridRouter.swift
//	Recruitment-iOS
//
//	Created by Bartłomiej Zabicki on 03/08/2019.
//	Copyright (c) 2019 Untitled Kingdom. All rights reserved.
//

import UIKit

@objc protocol ItemsGridRoutingLogic {
    func routeToItemDetails(segue: UIStoryboardSegue?)
}

protocol ItemsGridDataPassing {
	var dataStore: ItemsGridDataStore? { get }
}

class ItemsGridRouter: NSObject, ItemsGridRoutingLogic, ItemsGridDataPassing {

	// MARK: - Properties

	weak var viewController: ItemsGridViewController?
	var dataStore: ItemsGridDataStore?
	
	// MARK: - Routing
    
    func routeToItemDetails(segue: UIStoryboardSegue?) {
        guard let destinationVC = segue?.destination as? ItemDetailsViewController,
            var destinationDS = destinationVC.router?.dataStore,
            let dataStore = dataStore else { return }
        passDataToItemDetails(source: dataStore, destination: &destinationDS)
    }
    
    // MARK: - PassingData
    
    private func passDataToItemDetails(source: ItemsGridDataStore, destination: inout ItemDetailsDataStore) {
        destination.item = source.selectedItem
    }
	
}
