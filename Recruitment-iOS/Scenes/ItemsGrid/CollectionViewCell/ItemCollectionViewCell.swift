//
//  ItemCollectionViewCell.swift
//  Recruitment-iOS
//
//  Created by Bartłomiej Zabicki on 03/08/2019.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

import UIKit

class ItemCollectionViewCell: UICollectionViewCell {

    // MARK: - Structures
    
    struct ViewModel {
        let title: String
        let backgroundColor: UIColor
    }
    
    // MARK: - Outlets
    
    @IBOutlet weak var titleLabel: UILabel!
    
    // MARK: - Properties
    
    private(set) var viewModel: ViewModel?
    
    // MARK: - Functions
    
    func setupView(with viewModel: ViewModel) {
        self.viewModel = viewModel
        titleLabel.text = viewModel.title
        backgroundColor = viewModel.backgroundColor
    }

}
