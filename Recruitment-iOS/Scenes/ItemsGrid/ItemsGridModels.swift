//
//	ItemsGridModels.swift
//	Recruitment-iOS
//
//	Created by Bartłomiej Zabicki on 03/08/2019.
//	Copyright (c) 2019 Untitled Kingdom. All rights reserved.
//

import UIKit

enum ItemsGrid {
    
    enum FetchItems {
        struct Response {
            let items: [ItemModel]
            let delegate: ItemGridCollectionViewDelegate
        }
        struct ErrorResponse {
            let error: AppError
        }
        struct ViewModel {
            let delegateDataSource: ItemsGridCollectionViewDelegateDataSource
        }
        struct ErrorViewModel {
            let message: String
        }
    }
    
}
