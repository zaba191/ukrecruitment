//
//	ItemsGridViewController.swift
//	Recruitment-iOS
//
//	Created by Bartłomiej Zabicki on 03/08/2019.
//	Copyright (c) 2019 Untitled Kingdom. All rights reserved.
//

import UIKit

protocol ItemsGridDisplayLogic: class {
    func displayFetchItems(viewModel: ItemsGrid.FetchItems.ViewModel)
    func displayFetchItemsError(viewModel: ItemsGrid.FetchItems.ErrorViewModel)
    func displaySelectedItem()
}

class ItemsGridViewController: UIViewController, ItemsGridDisplayLogic {
    
    // MARK: - Outlets
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var messageLabel: UILabel!
    
    // MARK: - Properties
    
    var interactor: ItemsGridBusinessLogic?
    var router: (NSObjectProtocol & ItemsGridRoutingLogic & ItemsGridDataPassing)?
    
    // MARK: - Initialization
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    // MARK: - Setup
    
    private func setup() {
        let viewController = self
        let interactor = ItemsGridInteractor()
        let presenter = ItemsGridPresenter()
        let router = ItemsGridRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor
    }
    
    // MARK: - Routing
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let scene = segue.identifier {
            let selector = NSSelectorFromString("routeTo\(scene)WithSegue:")
            if let router = router, router.responds(to: selector) {
                router.perform(selector, with: segue)
            }
        }
    }
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCollectionViewLayout()
        interactor?.fetchItems()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setupTitle()
    }
    
    // MARK: - Display Logic
    
    func displayFetchItems(viewModel: ItemsGrid.FetchItems.ViewModel) {
        interactor?.assign(viewModel: viewModel)
        collectionView.dataSource = viewModel.delegateDataSource
        collectionView.delegate = viewModel.delegateDataSource
        collectionView.reloadData()
    }
    
    func displayFetchItemsError(viewModel: ItemsGrid.FetchItems.ErrorViewModel) {
        collectionView.fadeOut()
        messageLabel.text = viewModel.message
        messageLabel.fadeIn()
    }
    
    func displaySelectedItem() {
        performSegue(withIdentifier: "ItemDetails", sender: nil)
    }
    
    // MARK: - Private implementation
    
    private func setupTitle() {
        navigationController?.navigationBar.topItem?.title = "Grid".localized
    }
    
    private func setupCollectionViewLayout() {
        let spacing: CGFloat = 5
        let layout = UICollectionViewFlowLayout()
        let width = (collectionView.bounds.width - spacing) / 2
        layout.itemSize = CGSize(width: width, height: width)
        layout.minimumLineSpacing = spacing
        layout.minimumInteritemSpacing = spacing
        collectionView.setCollectionViewLayout(layout, animated: true)
    }
    
}
