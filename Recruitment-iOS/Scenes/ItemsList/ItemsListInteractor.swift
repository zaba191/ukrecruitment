//
//	ItemsListInteractor.swift
//	Recruitment-iOS
//
//	Created by Bartłomiej Zabicki on 03/08/2019.
//	Copyright (c) 2019 Untitled Kingdom. All rights reserved.
//

import UIKit

protocol ItemsListBusinessLogic {
    func fetchItems()
    func assign(viewModel: ItemsList.FetchItems.ViewModel)
}

protocol ItemsListDataStore: ItemsPersitenceStorable {
    var selectedItem: ItemModel? { get set }
}

class ItemsListInteractor: ItemsListBusinessLogic, ItemsListDataStore {

	// MARK: - Properties

	var presenter: ItemsListPresentationLogic?
    private var itemsTableViewDelegateDataSource: ItemListTableViewDelegateDataSource?
    
    var selectedItem: ItemModel?
    var itemsPersistenceStore: ItemsPersistenceStore!
	
	// MARK: - Business Logic
    
    func fetchItems() {
        itemsPersistenceStore.observe { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let items):
                let response = ItemsList.FetchItems.Response(items: items, delegate: self)
                self.presenter?.presentFetchItems(response: response)
            case .failure(let error):
                self.presenter?.presentFetchItemsError(response: .init(error: error))
            }
        }
    }
    
    func assign(viewModel: ItemsList.FetchItems.ViewModel) {
        itemsTableViewDelegateDataSource = viewModel.delegateDataSource
    }
    
    // MARK: - ItemsPersitenceStorable
    
    func assign(persistenceStore: ItemsPersistenceStore) {
        self.itemsPersistenceStore = persistenceStore
    }
	
}

extension ItemsListInteractor: ItemListTableViewDelegate {
    
    func didSelect(item: ItemModel) {
        selectedItem = item
        presenter?.presentSelectedItem()
    }
    
}
