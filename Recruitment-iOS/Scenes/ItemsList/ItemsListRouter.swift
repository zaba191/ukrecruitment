//
//	ItemsListRouter.swift
//	Recruitment-iOS
//
//	Created by Bartłomiej Zabicki on 03/08/2019.
//	Copyright (c) 2019 Untitled Kingdom. All rights reserved.
//

import UIKit

@objc protocol ItemsListRoutingLogic {
    func routeToItemDetails(segue: UIStoryboardSegue?)
}

protocol ItemsListDataPassing {
	var dataStore: ItemsListDataStore? { get }
}

class ItemsListRouter: NSObject, ItemsListRoutingLogic, ItemsListDataPassing {

	// MARK: - Properties

	weak var viewController: ItemsListViewController?
	var dataStore: ItemsListDataStore?
	
	// MARK: - Routing
    
    func routeToItemDetails(segue: UIStoryboardSegue?) {
        guard let destinationVC = segue?.destination as? ItemDetailsViewController,
            var destinationDS = destinationVC.router?.dataStore,
            let dataStore = dataStore else { return }
        passDataToItemDetails(source: dataStore, destination: &destinationDS)
    }
    
    // MARK: - PassingData
    
    private func passDataToItemDetails(source: ItemsListDataStore, destination: inout ItemDetailsDataStore) {
        destination.item = source.selectedItem
    }
	
}
