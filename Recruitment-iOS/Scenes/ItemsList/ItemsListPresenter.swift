//
//	ItemsListPresenter.swift
//	Recruitment-iOS
//
//	Created by Bartłomiej Zabicki on 03/08/2019.
//	Copyright (c) 2019 Untitled Kingdom. All rights reserved.
//

import UIKit

protocol ItemsListPresentationLogic {
    func presentFetchItems(response: ItemsList.FetchItems.Response)
    func presentFetchItemsError(response: ItemsList.FetchItems.ErrorResponse)
    func presentSelectedItem()
}

class ItemsListPresenter: ItemsListPresentationLogic {

	// MARK: - Properties

	weak var viewController: ItemsListDisplayLogic?
	
	// MARK: - Presentation Logic
    
    func presentFetchItems(response: ItemsList.FetchItems.Response) {
        let delegateDataSource = ItemListTableViewDelegateDataSource(items: response.items,
                                                                     delegate: response.delegate)
        let viewModel = ItemsList.FetchItems.ViewModel(delegateDataSource: delegateDataSource)
        viewController?.displayFetchItems(viewModel: viewModel)
    }
    
    func presentFetchItemsError(response: ItemsList.FetchItems.ErrorResponse) {
        viewController?.displayFetchItemsError(viewModel: .init(message: response.error.localizedDescription))
    }
    
    func presentSelectedItem() {
        viewController?.displaySelectedItem()
    }

}
