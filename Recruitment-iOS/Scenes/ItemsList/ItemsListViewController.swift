//
//	ItemsListViewController.swift
//	Recruitment-iOS
//
//	Created by Bartłomiej Zabicki on 03/08/2019.
//	Copyright (c) 2019 Untitled Kingdom. All rights reserved.
//

import UIKit

protocol ItemsListDisplayLogic: class {
    func displayFetchItems(viewModel: ItemsList.FetchItems.ViewModel)
    func displayFetchItemsError(viewModel: ItemsList.FetchItems.ErrorViewModel)
    func displaySelectedItem()
}

class ItemsListViewController: UIViewController, ItemsListDisplayLogic {
    
    // MARK: - Outlets
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var messageLabel: UILabel!
    
	// MARK: - Properties

	var interactor: ItemsListBusinessLogic?
	var router: (NSObjectProtocol & ItemsListRoutingLogic & ItemsListDataPassing)?

	// MARK: - Initialization
	
	override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
		super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
		setup()
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		setup()
	}
    
	// MARK: - Setup
	
	private func setup() {
		let viewController = self
		let interactor = ItemsListInteractor()
		let presenter = ItemsListPresenter()
		let router = ItemsListRouter()
		viewController.interactor = interactor
		viewController.router = router
		interactor.presenter = presenter
		presenter.viewController = viewController
		router.viewController = viewController
		router.dataStore = interactor
	}
	
	// MARK: - Routing
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if let scene = segue.identifier {
			let selector = NSSelectorFromString("routeTo\(scene)WithSegue:")
			if let router = router, router.responds(to: selector) {
				router.perform(selector, with: segue)
			}
		}
	}
	
	// MARK: - Lifecycle
	
	override func viewDidLoad() {
		super.viewDidLoad()
        interactor?.fetchItems()
	}
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setupTitle()
    }

	// MARK: - Display Logic
    
    func displayFetchItems(viewModel: ItemsList.FetchItems.ViewModel) {
        interactor?.assign(viewModel: viewModel)
        tableView.dataSource = viewModel.delegateDataSource
        tableView.delegate = viewModel.delegateDataSource
        tableView.reloadData()
    }
    
    func displayFetchItemsError(viewModel: ItemsList.FetchItems.ErrorViewModel) {
        tableView.fadeOut()
        messageLabel.text = viewModel.message
        messageLabel.fadeIn()
    }
    
    func displaySelectedItem() {
        performSegue(withIdentifier: "ItemDetails", sender: nil)
    }
    
    // MARK: - Private implementation
    
    private func setupTitle() {
        navigationController?.navigationBar.topItem?.title = "List".localized
    }
	
}
