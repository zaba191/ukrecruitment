//
//  ItemsListTableViewDelegate+DataSource.swift
//  Recruitment-iOS
//
//  Created by Bartłomiej Zabicki on 03/08/2019.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

import UIKit

protocol ItemListTableViewDelegate: class {
    func didSelect(item: ItemModel)
}

final class ItemListTableViewDelegateDataSource: NSObject, UITableViewDataSource {
    
    // MARK: - Properties
    
    var items: [ItemModel]
    weak var delegate: ItemListTableViewDelegate?
    
    // MARK: - Initialization
    
    init(items: [ItemModel], delegate: ItemListTableViewDelegate) {
        self.items = items
        self.delegate = delegate
    }
    
    // MARK: - UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusable(cell: ItemTableViewCell.self, for: indexPath)
        let item = items[indexPath.row]
        
        let viewModel = ItemTableViewCell.ViewModel(title: item.name,
                                                    preview: item.preview,
                                                    backgroundColor: item.color)
        cell.setupView(with: viewModel)
        return cell
    }
}

// MARK: - UITableViewDelegate

extension ItemListTableViewDelegateDataSource: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedItem = items[indexPath.row]
        delegate?.didSelect(item: selectedItem)
    }
    
}
