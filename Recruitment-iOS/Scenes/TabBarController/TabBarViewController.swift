//
//  TabBarViewController.swift
//  Recruitment-iOS
//
//  Created by Bartłomiej Zabicki on 03/08/2019.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

import UIKit

class TabBarViewController: UITabBarController {
    
    // MARK: - Properties
    
    lazy var persistenceStore: ItemsPersistenceStore = {
        return ItemsPersistenceStore()
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        createViewControllers()
    }
    
    // MARK: - Private implementation
    
    private func createViewControllers() {
        let itemsStoryboard = UIStoryboard(name: "Items", bundle: nil)
        
        let listViewController = itemsStoryboard.instantiateViewController(type: ItemsListViewController.self)
        let listViewDataStore = listViewController.router?.dataStore
        listViewDataStore?.assign(persistenceStore: persistenceStore)
        

        let gridViewController = itemsStoryboard.instantiateViewController(type: ItemsGridViewController.self)
        let gridViewDataStore = gridViewController.router?.dataStore
        gridViewDataStore?.assign(persistenceStore: persistenceStore)
        
        setViewControllers([listViewController, gridViewController], animated: true)
    }

}
