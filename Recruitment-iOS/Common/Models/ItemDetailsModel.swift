//
//  ItemDetailsModel.swift
//  UKiOSTest
//
//  Created by Paweł Sporysz on 15.09.2016.
//  Copyright © 2016 Paweł Sporysz. All rights reserved.
//

import UIKit

struct ItemDetailsModel {
    
    // MARK: - Properties
    
    let id: String
    let desc: String
}

// MARK: - Decodable

extension ItemDetailsModel: Decodable {
    
    enum CodingKeys: String, CodingKey {
        case id
        case attributes
    }
    
    enum AttributesCodingKeys: String, CodingKey {
        case desc
    }
    
    // MARK: - Codable Initialization
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decode(String.self, forKey: .id)
        
        let attributesContainer = try container.nestedContainer(keyedBy: AttributesCodingKeys.self,
                                                                forKey: .attributes)
        desc = try attributesContainer.decode(String.self, forKey: .desc)
    }
    
}
