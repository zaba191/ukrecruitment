//
//  AppError.swift
//  Recruitment-iOS
//
//  Created by Bartłomiej Zabicki on 03/08/2019.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

import Foundation
import Unicorns

public struct AppError: Error, LocalizedError {
    
    // MARK: - Properties
    
    var code: Int
    var localizedDescription: String
    
    static let somethingWentWrong = AppError(description: "SomethingWentWrong")
    
    // MARK: - Initialization
    
    init(from networkError: NetworkingError) {
        self.code = networkError.code
        self.localizedDescription = networkError.description
    }
    
    init(code: Int, description: String) {
        self.code = code
        self.localizedDescription = description.localized
    }
    
    init(description: String) {
        self.code = 0
        self.localizedDescription = description.localized
    }
    
    init(error: Error) {
        if let networkingError = error as? NetworkingError {
            self.code = networkingError.code
            self.localizedDescription = networkingError.description
        } else if let appError = error as? AppError {
            self.code = appError.code
            self.localizedDescription = appError.localizedDescription
        } else if let decodingError = error as? DecodingError {
            self.code = 0
            self.localizedDescription = decodingError.localizedDescription
        } else {
            self.code = 0
            self.localizedDescription = error.localizedDescription
        }
    }
    
}
