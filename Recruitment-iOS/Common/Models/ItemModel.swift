//
//  ItemModel.swift
//  UKiOSTest
//
//  Created by Paweł Sporysz on 15.09.2016.
//  Copyright © 2016 Paweł Sporysz. All rights reserved.
//

import UIKit

struct ItemModel {
    
    // MARK: - Properties
    
    let id: String
    let name: String
    let color: UIColor
    let preview: String
    
}

// MARK: - Decodable

extension ItemModel: Decodable {
    
    // MARK: - Enums
    
    enum CodingKeys: String, CodingKey {
        case id
        case attributes
    }
    
    enum AttributesCodingKeys: String, CodingKey {
        case name
        case color
        case preview
    }
    
    // MARK: - Codable Initialization
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decode(String.self, forKey: .id)
        
        let attributesContainer = try container.nestedContainer(keyedBy: AttributesCodingKeys.self,
                                                                forKey: .attributes)
        name = try attributesContainer.decode(String.self, forKey: .name)
        color = try attributesContainer.decodeColor(forKey: .color)
        preview = try attributesContainer.decode(String.self, forKey: .preview)
    }
}
