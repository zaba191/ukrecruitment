//
//  Endpoint.swift
//  Recruitment-iOS
//
//  Created by Bartłomiej Zabicki on 03/08/2019.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

import UIKit
import Unicorns

public struct Endpoint {
    
    var url: URL? {
        var components = URLComponents(string: "")
        components?.path = path
        components?.queryItems = queryItems
        return components?.url
    }
    
    let path: String
    let queryItems: [URLQueryItem]
    let method: Networking.NetworkMethod
    let parameters: JSON?
    let headers: JSON?
    let encoding: Networking.Encoding
    
    init(path: String,
         queryItems: [URLQueryItem] = [],
         method: Networking.NetworkMethod,
         parameters: JSON? = nil,
         headers: JSON? = nil,
         encoding: Networking.Encoding = .json) {
        self.path = path
        self.queryItems = queryItems
        self.method = method
        self.parameters = parameters
        self.headers = headers
        self.encoding = encoding
    }
    
}

extension Networking {
    
    public func request<T: Decodable>(endpoint: Endpoint,
                                    jsonDecoder: JSONDecoder = JSONDecoder(),
                                    onSuccess: @escaping (T) -> Void,
                                    onError: @escaping (AppError) -> Void) {
        
        guard let url = endpoint.url else {
            return onError(AppError.init(description: "Invalid url"))
        }
        
        Networking.shared.request(with: url,
                                  method: endpoint.method,
                                  parameters: endpoint.parameters,
                                  headers: endpoint.headers,
                                  encoding: endpoint.encoding,
                                  onSuccess: { data in
                                    do {
                                        let result = try jsonDecoder.decode(T.self, from: data)
                                        onSuccess(result)
                                    } catch let error {
                                        Log.e("Cannot decode as \(type(of: T.self))")
                                        if let decodingError = error as? DecodingError {
                                            Log.e("\(String(describing: decodingError._userInfo))")
                                            let error = AppError(error: decodingError)
                                            onError(error)
                                        } else {
                                            let decodingData = String(describing: String(data: data, encoding: .utf8))
                                            Log.e(decodingData)
                                            let error = AppError(error: error)
                                            onError(error)
                                        }
                                    }
        }, onError: { error in
            let error = AppError(error: error)
            onError(error)
        })
    }
    
}
