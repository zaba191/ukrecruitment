//
//  Endpoints.swift
//  Recruitment-iOS
//
//  Created by Bartłomiej Zabicki on 03/08/2019.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

import Foundation

extension Endpoint {
    
    static func items() -> Endpoint {
        return Endpoint(path: "Items", method: .get)
    }
    
    static func item(id: String) -> Endpoint {
        return Endpoint(path: "Item\(id)", method: .get)
    }
    
}
