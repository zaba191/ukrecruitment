//
//  MockURLSession.swift
//  Recruitment-iOS
//
//  Created by Bartłomiej Zabicki on 03/08/2019.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

import Unicorns

class MockURLSession: URLSessionProtocol {
    
    // MARK: - Properties
    
    var dataTask = MockURLSessionDataTask()
    private (set) var expectedJSON: String
    private (set) var expectedStatusCode: Int
    static var responseDelay = 2.0
    
    // MARK: - Initializationz
    
    init(expectedJSON: String, expectedStatusCode: Int = 200) {
        self.expectedJSON = expectedJSON
        self.expectedStatusCode = expectedStatusCode
    }
    
    // MARK: - Functions
    
    func dataTask(with url: URL, completionHandler: @escaping DataTaskResult) -> URLSessionDataTaskProtocol {
        let response = HTTPURLResponse(url: url, statusCode: expectedStatusCode, httpVersion: nil, headerFields: nil)
        let expectedJSON = self.expectedJSON
        if MockURLSession.responseDelay != 0 {
            DispatchQueue.main.asyncAfter(deadline: .now() + MockURLSession.responseDelay) {
                completionHandler(expectedJSON.data(using: .utf8), response, nil)
            }
        } else {
            completionHandler(expectedJSON.data(using: .utf8), response, nil)
        }
        return dataTask
    }
    
    func dataTask(with request: URLRequest, completionHandler: @escaping DataTaskResult) -> URLSessionDataTaskProtocol {
        guard let url = request.url else {
            return dataTask
        }
        return dataTask(with: url, completionHandler: completionHandler)
    }
    
}

class MockURLSessionDataTask: URLSessionDataTaskProtocol {
    
    private (set) var resumeWasCalled = false
    
    func resume() {
        resumeWasCalled = true
    }
    
}
