//
//  JSONMockManager.swift
//  Recruitment-iOS
//
//  Created by Bartłomiej Zabicki on 03/08/2019.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

import Foundation
import Unicorns

final class JSONMocksManager {
    
    // MARK: - Initialization
    
    private init() {}
    
    // MARK: - Enums
    
    static func json(from filename:String) -> String {
        guard let fileURL = Bundle.main.url(forResource: filename, withExtension: "json") else { return "" }
        do {
            let stringContent = try String(contentsOf: fileURL)
            if let data = stringContent.data(using: .utf8),
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: []) as? JSON {
                
                if let value = jsonResult["data"] {
                    let decodedValue = try JSONSerialization.data(withJSONObject: value, options: [])
                    return String(data: decodedValue, encoding: .utf8) ?? ""
                }
                return ""
            } else {
                return ""
            }
        } catch {
            return ""
        }
    }
    
}

