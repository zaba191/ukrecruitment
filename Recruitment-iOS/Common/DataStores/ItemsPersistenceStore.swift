//
//  ItemsPersistenceStore.swift
//  Recruitment-iOS
//
//  Created by Bartłomiej Zabicki on 03/08/2019.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

import Foundation

protocol ItemsPersitenceStorable: class {
    var itemsPersistenceStore: ItemsPersistenceStore! { get set }
    func assign(persistenceStore: ItemsPersistenceStore)
}

class ItemsPersistenceStore {
    
    typealias ItemsResult = Result<[ItemModel], AppError>
    
    // MARK: - Properties
    
    private let worker = ItemsWorker()
    private(set) var completions: [(ItemsResult) -> Void] = []
    private(set) var items: ItemsResult? {
        didSet {
            items.map { report(result: $0) }
        }
    }
    private var isFetching = false
    
    // MARK: - Functions
    
    func observe(with callback: @escaping (ItemsResult) -> Void) {
        completions.append(callback)
        items.map(callback)
        fetchItemsIfNecessary()
    }
    
    // MARK: - Private implementation
    
    private func report(result: ItemsResult) {
        completions.forEach({ completion in
            completion(result)
        })
        completions.removeAll()
    }
    
    private func fetchItemsIfNecessary() {
        guard items == nil, !isFetching else {
            Log.i("Items are already fetched or fetching")
            return
        }
        isFetching = true
        worker.fetchItems(onSuccess: { [weak self] items in
            guard let self = self else { return }
            self.items = .success(items)
            self.isFetching = false
        }, onError: { [weak self] error in
            guard let self = self else { return }
            self.items = .failure(error)
            self.isFetching = false
        })
    }
    
}
