//
//	ItemsListWorker.swift
//	Recruitment-iOS
//
//	Created by Bartłomiej Zabicki on 03/08/2019.
//	Copyright (c) 2019 Untitled Kingdom. All rights reserved.
//

import UIKit
import Unicorns

class ItemsWorker {
    
    func fetchItems(onSuccess: @escaping ([ItemModel]) -> Void,
                    onError: @escaping (AppError) -> Void) {
        
        let endpoint = Endpoint.items()
        Networking.session = MockURLSession(expectedJSON: JSONMocksManager.json(from: endpoint.path), expectedStatusCode: 200)
        Networking.shared.request(endpoint: .items(), onSuccess: onSuccess, onError: onError)
    }
    
}
