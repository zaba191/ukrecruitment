//
//  StringExtension.swift
//  Recruitment-iOS
//
//  Created by Bartłomiej Zabicki on 03/08/2019.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

import Foundation

extension String {
    
    func uppercasedLetter(every count: Int) -> String {
        return enumerated()
            .compactMap({ $0.offset % count == 0 ? $0.element.uppercased() : String($0.element)})
            .joined()
    }
    
}
