//
//  KeyedDecodingContainerExtension.swift
//  Recruitment-iOS
//
//  Created by Bartłomiej Zabicki on 03/08/2019.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

import UIKit

extension KeyedDecodingContainer {

    enum DecodableColors: String, Decodable {
        case red = "Red"
        case green = "Green"
        case blue = "Blue"
        case yellow = "Yellow"
        case purple = "Purple"
        
        var uiColor: UIColor {
            switch self {
            case .red: return .red
            case .green: return .green
            case .blue: return .blue
            case .yellow: return .yellow
            case .purple: return .purple
            }
        }
    }
    
    func decodeColor(forKey key: KeyedDecodingContainer.Key) throws -> UIColor {
        return try decode(DecodableColors.self, forKey: key).uiColor
    }
}
