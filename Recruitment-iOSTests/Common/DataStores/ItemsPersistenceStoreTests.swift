//
//  ItemsPersistenceStoreTests.swift
//  Recruitment-iOSTests
//
//  Created by Bartłomiej Zabicki on 04/08/2019.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

@testable import Recruitment_iOS
import XCTest

class ItemsPersistenceStoreTests: XCTestCase {
    
    // MARK: - Properties
    
    var sut: ItemsPersistenceStore!
    
    // MARK: - Setup

    override func setUp() {
        MockURLSession.responseDelay = 0
        sut = ItemsPersistenceStore()
    }
    
    // MARK: - Tests
    
    func testFetchingItems() {
        var areItemsFetched = false
        sut.observe { _ in
            areItemsFetched = true
        }
        
        XCTAssert(areItemsFetched, "observe should fetch or return current items")
    }
    
    func testFetchingItemsFromMultipleSources() {
        let sourcesToTest = 2
        var fetchedCounter = 0
        
        for _ in 0..<sourcesToTest {
            sut.observe { _ in
                fetchedCounter += 1
            }
        }
        
        XCTAssert(fetchedCounter == sourcesToTest, "observe should fetch and return data to all subscribens")
    }

}
