//
//  StringExtensionTests.swift
//  Recruitment-iOSTests
//
//  Created by Bartłomiej Zabicki on 04/08/2019.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

@testable import Recruitment_iOS
import XCTest

class StringExtensionTests: XCTestCase {

    func testUppercasedLettersEveryX() {
        let expectedString = "AbCdEfGh"
        let givenString = "abcdefgh"
        let modifiedString = givenString.uppercasedLetter(every: 2)
        
        XCTAssert(expectedString == modifiedString, "result string is different that expected")
    }

}
