//
//	ItemDetailsPresenterTests.swift
//	Recruitment-iOS
//
//	Created by Bartłomiej Zabicki on 03/08/2019.
//	Copyright (c) 2019 Untitled Kingdom. All rights reserved.
//

@testable import Recruitment_iOS
import XCTest

class ItemDetailsPresenterTests: XCTestCase {

	// MARK: - Subject under test
	
	var sut: ItemDetailsPresenter!
    var viewController: ItemDetailsDisplayLogicSpy!
	
	// MARK: - Test lifecycle
	
	override func setUp() {
		super.setUp()
		setupItemDetailsPresenter()
	}
	
	override func tearDown() {
		super.tearDown()
	}
	
	// MARK: - Test setup
	
	private func setupItemDetailsPresenter() {
		sut = ItemDetailsPresenter()
        viewController = ItemDetailsDisplayLogicSpy()
        sut.viewController = viewController
	}
	
	// MARK: - Test spyes
	
    class ItemDetailsDisplayLogicSpy: ItemDetailsDisplayLogic {
        
        var isDetailsSuccessDisplayed = false
        var isDetailsErrorDisplayed = false
        var isItemDisplayed = false
        
        func displayFetchDetails(viewModel: ItemDetails.FetchDetails.ViewModel) {
            isDetailsSuccessDisplayed = true
        }
        
        func displayFetchDetailsError(viewModel: ItemDetails.FetchDetails.ErrorViewModel) {
            isDetailsErrorDisplayed = true
        }
        
        func displayItem(viewModel: ItemDetails.PrepareItem.ViewModel) {
            isItemDisplayed = true
        }
    }
	
	// MARK: - Tests
	
    func testPresentDetails() {
        let mockedModel = ItemDetailsModel(id: "1", desc: "Example description")
        let response = ItemDetails.FetchDetails.Response(details: mockedModel)
        
        sut.presentFetchDetails(response: response)
        
        XCTAssert(viewController.isDetailsSuccessDisplayed, "presentFetchDetails should display item details")
    }
    
    func testPresentDetailsError() {
        let mockedError = AppError(description: "Error")
        let response = ItemDetails.FetchDetails.ErrorResponse(error: mockedError)
        
        sut.presentFetchDetailsError(response: response)
        
        XCTAssert(viewController.isDetailsErrorDisplayed, "presentFetchDetailsError should display error")
    }
    
    func testPresentItem() {
        let mockedModel = ItemModel(id: "0", name: "testName", color: .white, preview: "testPreview")
        sut.presentItem(response: .init(item: mockedModel))
        
        XCTAssert(viewController.isItemDisplayed, "presentFetchDetails should display current item")
    }
}
