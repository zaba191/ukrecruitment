//
//	ItemDetailsInteractorTests.swift
//	Recruitment-iOS
//
//	Created by Bartłomiej Zabicki on 03/08/2019.
//	Copyright (c) 2019 Untitled Kingdom. All rights reserved.
//

@testable import Recruitment_iOS
import XCTest
import Unicorns

class ItemDetailsInteractorTests: XCTestCase {

	// MARK: - Subject under test
	
	var sut: ItemDetailsInteractor!
    var presenter: ItemDetailsPresentationLogicSpy!
	
	// MARK: - Test lifecycle
	
	override func setUp() {
		super.setUp()
		setupItemDetailsInteractor()
	}
	
	override func tearDown() {
		super.tearDown()
	}
	
	// MARK: - Test setup
	
	func setupItemDetailsInteractor() {
        MockURLSession.responseDelay = 0
		sut = ItemDetailsInteractor()
        presenter = ItemDetailsPresentationLogicSpy()
        sut.presenter = presenter
	}
	
	// MARK: - Test spyes
	
    class ItemDetailsPresentationLogicSpy: ItemDetailsPresentationLogic {
        
        var areDetailsPresented = false
        var isItemPresented = false
        
        func presentFetchDetails(response: ItemDetails.FetchDetails.Response) {
            areDetailsPresented = true 
        }
        
        ///Testing this func currently is unneccessary because the only thing that can fail now is encoding and if that happend, the test above will fail. Additionaly we don't have to mock our data because they're already mocked in json file, so no injection is neccessary. If that whould be case we would mock Networking.Session to override response
        func presentFetchDetailsError(response: ItemDetails.FetchDetails.ErrorResponse) {}
        
        func presentItem(response: ItemDetails.PrepareItem.Response) {
            isItemPresented = true
        }
    }
	
	// MARK: - Tests
    
    func testFetchDetailsSuccess() {
        sut.item = ItemModel(id: "1", name: "", color: .red, preview: "")
        sut.fetchDetails()
        XCTAssert(presenter.areDetailsPresented, "fetchDetails() should present details")
    }
    
    func testPresentItem() {
        sut.item = ItemModel(id: "1", name: "", color: .red, preview: "")
        sut.prepareView()
        XCTAssert(presenter.isItemPresented, "prepareView should invoke item presentation")
    }
    
}
