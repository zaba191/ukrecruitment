//
//	ItemDetailsWorkerTests.swift
//	Recruitment-iOS
//
//	Created by Bartłomiej Zabicki on 03/08/2019.
//	Copyright (c) 2019 Untitled Kingdom. All rights reserved.
//

@testable import Recruitment_iOS
import XCTest

class ItemDetailsWorkerTests: XCTestCase {

	// MARK: - Subject under test
	
	var sut: ItemDetailsWorker!
	
	// MARK: - Test lifecycle
	
	override func setUp() {
		super.setUp()
		setupItemDetailsWorker()
	}
	
	override func tearDown() {
		super.tearDown()
	}
	
	// MARK: - Test setup
	
	func setupItemDetailsWorker() {
        MockURLSession.responseDelay = 0
		sut = ItemDetailsWorker()
	}
	// MARK: - Tests
    
    func testFetchingDetails() {
        var areDetailsFetched = false
        
        let request = ItemDetails.FetchDetails.WorkerRequest(id: "1")
        sut.fetchItemDetails(request: request, onSuccess: { detailsModel in
            areDetailsFetched = true
        }, onError: {_ in })
        
        XCTAssert(areDetailsFetched, "fetchItemDetails should fetch model of item")
    }
    
    func testFetchingDetailsCorrectItem() {
        var isFetchedCorrectItem = false
        let idToFetch = "1"
        
        let request = ItemDetails.FetchDetails.WorkerRequest(id: idToFetch)
        sut.fetchItemDetails(request: request, onSuccess: { detailsModel in
            isFetchedCorrectItem = detailsModel.id == idToFetch
        }, onError: {_ in })
        
        XCTAssert(isFetchedCorrectItem, "fetchItemDetails should fetch model of Item1")
    }
}
