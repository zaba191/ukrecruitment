//
//	ItemsGridInteractorTests.swift
//	Recruitment-iOS
//
//	Created by Bartłomiej Zabicki on 03/08/2019.
//	Copyright (c) 2019 Untitled Kingdom. All rights reserved.
//

@testable import Recruitment_iOS
import XCTest

class ItemsGridInteractorTests: XCTestCase {

	// MARK: - Subject under test
	
	var sut: ItemsGridInteractor!
    var presenter: ItemsGridPresentationLogicSpy!
	
	// MARK: - Test lifecycle
	
	override func setUp() {
		super.setUp()
		setupItemsGridInteractor()
	}
	
	override func tearDown() {
		super.tearDown()
	}
	
	// MARK: - Test setup
	
	func setupItemsGridInteractor() {
        MockURLSession.responseDelay = 0
		sut = ItemsGridInteractor()
        presenter = ItemsGridPresentationLogicSpy()
        sut.presenter = presenter
	}
	
	// MARK: - Test spyes
	
    class ItemsGridPresentationLogicSpy: ItemsGridPresentationLogic {
        
        var areItemsPresented = false
        var isSelectedItemPresented = false
        
        func presentFetchItems(response: ItemsGrid.FetchItems.Response) {
            areItemsPresented = true
        }
        
        func presentFetchItemsError(response: ItemsGrid.FetchItems.ErrorResponse) {}
        
        func presentSelectedItem() {
            isSelectedItemPresented = true
        }
    }
	
	// MARK: - Tests
    
    func testFetchDetailsSuccess() {
        sut.itemsPersistenceStore = ItemsPersistenceStore()
        sut.fetchItems()
        
        XCTAssert(presenter.areItemsPresented, "fetchItems() should present items")
    }
    
    func testPresentItem() {
        let item = ItemModel(id: "1", name: "", color: .red, preview: "")
        sut.didSelect(item: item)
        XCTAssert(presenter.isSelectedItemPresented, "didSelect should invoke item presentation")
    }
}
