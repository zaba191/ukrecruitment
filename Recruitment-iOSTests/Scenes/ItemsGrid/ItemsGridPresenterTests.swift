//
//	ItemsGridPresenterTests.swift
//	Recruitment-iOS
//
//	Created by Bartłomiej Zabicki on 03/08/2019.
//	Copyright (c) 2019 Untitled Kingdom. All rights reserved.
//

@testable import Recruitment_iOS
import XCTest

class ItemsGridPresenterTests: XCTestCase {

	// MARK: - Subject under test
	
	var sut: ItemsGridPresenter!
    var viewController: ItemsGridDisplayLogicSpy!
	
	// MARK: - Test lifecycle
	
	override func setUp() {
		super.setUp()
		setupItemsGridPresenter()
	}
	
	override func tearDown() {
		super.tearDown()
	}
	
	// MARK: - Test setup
	
	func setupItemsGridPresenter() {
		sut = ItemsGridPresenter()
        viewController = ItemsGridDisplayLogicSpy()
        sut.viewController = viewController
	}
	
	// MARK: - Test spyes
	
    class ItemsGridDisplayLogicSpy: ItemsGridDisplayLogic {
        
        var areItemsDisplayed = false
        var isErrorDisplayed = false
        var isItemDisplayed = false
        
        func displayFetchItems(viewModel: ItemsGrid.FetchItems.ViewModel) {
            areItemsDisplayed = true
        }
        
        func displayFetchItemsError(viewModel: ItemsGrid.FetchItems.ErrorViewModel) {
            isErrorDisplayed = true
        }
        
        func displaySelectedItem() {
            isItemDisplayed = true
        }
    }
    
    class GridDelegateSpy: ItemGridCollectionViewDelegate {
        
        func didSelect(item: ItemModel) {}
    }
	
	// MARK: - Tests
	
    func testDisplayFetchItems() {
        let delegateSpy = GridDelegateSpy()
        let response = ItemsGrid.FetchItems.Response(items: [], delegate: delegateSpy)
        sut.presentFetchItems(response: response)
        
        XCTAssert(viewController.areItemsDisplayed, "presentFetchItems should display grid")
    }
    
    func testDisplayFetchItemsError() {
        let error = AppError.somethingWentWrong
        sut.presentFetchItemsError(response: .init(error: error))
        
        XCTAssert(viewController.isErrorDisplayed, "presentFetchItemsError should display error")
    }
    
    func testSelectItem() {
        sut.presentSelectedItem()
        XCTAssert(viewController.isItemDisplayed, "presentSelectedItem should display this item")
    }
}
