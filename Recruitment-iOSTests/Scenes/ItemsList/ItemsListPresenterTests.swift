//
//	ItemsListPresenterTests.swift
//	Recruitment-iOS
//
//	Created by Bartłomiej Zabicki on 03/08/2019.
//	Copyright (c) 2019 Untitled Kingdom. All rights reserved.
//

@testable import Recruitment_iOS
import XCTest

class ItemsListPresenterTests: XCTestCase {
    
    // MARK: - Subject under test
    
    var sut: ItemsListPresenter!
    var viewController = ItemsListDisplayLogicSpy()
    
    // MARK: - Test lifecycle
    
    override func setUp() {
        super.setUp()
        setupItemsListPresenter()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    // MARK: - Test setup
    
    func setupItemsListPresenter() {
        sut = ItemsListPresenter()
        viewController = .init()
        sut.viewController = viewController
    }
    
    // MARK: - Test spyes
    
    class ItemsListDisplayLogicSpy: ItemsListDisplayLogic {
        
        var areItemsDisplayed = false
        var isErrorDisplayed = false
        var isItemDisplayed = false
        
        func displayFetchItems(viewModel: ItemsList.FetchItems.ViewModel) {
            areItemsDisplayed = true
        }
        
        func displayFetchItemsError(viewModel: ItemsList.FetchItems.ErrorViewModel) {
            isErrorDisplayed = true
        }
        
        func displaySelectedItem() {
            isItemDisplayed = true
        }
    }
    
    class ListDelegateSpy: ItemListTableViewDelegate {
        
        func didSelect(item: ItemModel) {}
    }
    
    // MARK: - Tests
    
    func testDisplayFetchItems() {
        let delegateSpy = ListDelegateSpy()
        let response = ItemsList.FetchItems.Response(items: [], delegate: delegateSpy)
        sut.presentFetchItems(response: response)
        
        XCTAssert(viewController.areItemsDisplayed, "presentFetchItems should display grid")
    }
    
    func testDisplayFetchItemsError() {
        let error = AppError.somethingWentWrong
        sut.presentFetchItemsError(response: .init(error: error))
        
        XCTAssert(viewController.isErrorDisplayed, "presentFetchItemsError should display error")
    }
    
    func testSelectItem() {
        sut.presentSelectedItem()
        XCTAssert(viewController.isItemDisplayed, "presentSelectedItem should display this item")
    }
}
