//
//	ItemsListInteractorTests.swift
//	Recruitment-iOS
//
//	Created by Bartłomiej Zabicki on 03/08/2019.
//	Copyright (c) 2019 Untitled Kingdom. All rights reserved.
//

@testable import Recruitment_iOS
import XCTest

class ItemsListInteractorTests: XCTestCase {

	// MARK: - Subject under test
	
	var sut: ItemsListInteractor!
    var presenter: ItemsListPresentationLogicSpy!
	
	// MARK: - Test lifecycle
	
	override func setUp() {
		super.setUp()
		setupItemsListInteractor()
	}
	
	override func tearDown() {
		super.tearDown()
	}
	
	// MARK: - Test setup
	
	func setupItemsListInteractor() {
        MockURLSession.responseDelay = 0
		sut = ItemsListInteractor()
        presenter = .init()
        sut.presenter = presenter
	}
	
	// MARK: - Test spyes
	
	class ItemsListPresentationLogicSpy: ItemsListPresentationLogic {
        
        var areItemsPresented = false
        var isSelectedItemPresented = false
        
        func presentFetchItems(response: ItemsList.FetchItems.Response) {
            areItemsPresented = true
        }
        
        func presentFetchItemsError(response: ItemsList.FetchItems.ErrorResponse) {}
        
        func presentSelectedItem() {
            isSelectedItemPresented = true
        }
        
    }
	
	// MARK: - Tests
    
    func testFetchDetailsSuccess() {
        sut.itemsPersistenceStore = ItemsPersistenceStore()
        sut.fetchItems()
        
        XCTAssert(presenter.areItemsPresented, "fetchItems() should present items")
    }
    
    func testPresentItem() {
        let item = ItemModel(id: "1", name: "", color: .red, preview: "")
        sut.didSelect(item: item)
        XCTAssert(presenter.isSelectedItemPresented, "didSelect should invoke item presentation")
    }
}
